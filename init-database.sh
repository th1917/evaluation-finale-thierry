#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER zira_user ENCRYPTED PASSWORD 'zira_pass';
    CREATE DATABASE evaluation OWNER zira_user;
    GRANT ALL PRIVILEGES ON DATABASE evaluation TO zira_user;
    GRANT ALL ON schema public TO zira_user;
EOSQL
