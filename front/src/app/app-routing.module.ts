import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { AddProjectComponent } from "./add-project/add-project.component";
import { TicketDetailComponent } from "./ticket-detail/ticket-detail.component";
import { TicketListComponent } from "./ticket-list/ticket-list.component";
import { AddTicketComponent } from "./add-ticket/add-ticket.component";
import { ProjectDetailComponent } from "./project-detail/project-detail.component";
import { UsersDetailComponent } from "./users-detail/users-detail.component";
import { UsersListComponent } from "./users-list/users-list.component";
import { UpdateTicketComponent } from "./update-ticket/update-ticket.component";
import { AddUsersComponent } from "./add-users/add-users.component";
import { UpdateUsersComponent } from "./update-users/update-users.component";
import { UpdateProjectComponent } from "./update-project/update-project.component";


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' }, // redirige les chemins '/' vers la page l'accueil '/home'
    { path: 'home', component: HomeComponent },
    {path : 'projects/:id/addTicket' , component : AddTicketComponent},
    {path : 'users/:id/addProject' , component : AddProjectComponent},
    {path : 'addProject' , component : AddProjectComponent},
    {path : 'ticketdetail/:id' , component : TicketDetailComponent},
    {path : 'projectdetail/:id' , component : ProjectDetailComponent},
    {path : 'ticketstatus' , component : TicketListComponent},
    {path : 'usersdetail/:id' , component : UsersDetailComponent},
    {path : 'users' , component : UsersListComponent},
    {path : 'addUsers' , component : AddUsersComponent},
    {path : 'updateTicket/:id' , component : UpdateTicketComponent},
    {path : 'updateProject/:id' , component : UpdateProjectComponent},
    {path : 'updateUsers/:id' , component : UpdateUsersComponent},
  ]
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })
  export class AppRoutingModule {}
  