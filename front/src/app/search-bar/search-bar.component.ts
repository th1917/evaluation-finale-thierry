import { Component, EventEmitter, Output } from '@angular/core';
import { ProjectService } from '../service/project.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {
  recherche :string = ''; 

  @Output() search = new EventEmitter<string>();
  
  constructor(private _projectService :ProjectService){}
  
  
  searchProjects(event :any){
    this.recherche = event?.target?.value?.toLowerCase();
    this._projectService.searchProject(this.recherche).subscribe(project=>{console.log(project)});
    console.log(this.recherche);
  }

  searchProjectsUsers(event :any){
    this.recherche = event?.target?.value?.toLowerCase();
    this._projectService.searchProjectUsers(this.recherche).subscribe(project=>{console.log(project)});
    console.log(this.recherche);
  }
  
}
