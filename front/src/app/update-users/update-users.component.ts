import { Component } from '@angular/core';
import { Users } from '../models/users';
import { ProjectService } from '../service/project.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-users',
  templateUrl: './update-users.component.html',
  styleUrls: ['./update-users.component.scss']
})
export class UpdateUsersComponent {
  usersId : string = ''
  users?: Users;

  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  usersForm = new FormGroup({
    firstName: new FormControl('',Validators.required),
    lastName: new FormControl('')
  });

  //recupération du users par rapport à son id et affectation des valeur dans le formulaire
  ngOnInit(): void {

    this.usersId = this.route.snapshot.paramMap.get('id') as string

    this._projectService.getUsersById(this.usersId).subscribe(users => {
      this.users = users;

      if (users) {
        this.usersForm.patchValue({
          firstName: this.users.firstName,
          lastName: this.users.lastName
        })
      }
    })

  }

  async save() {
    if (this.usersForm.valid) {
      const values = this.usersForm.value 
      const result = {...values} as unknown as Users
      this._projectService.updateUsers(result, this.usersId).subscribe()
      this._router.navigateByUrl('/users');
    }
  }
}

