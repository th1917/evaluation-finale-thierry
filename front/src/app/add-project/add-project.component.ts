import { Component } from '@angular/core';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from '../models/project';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent {
  usersId! : string
  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  projectForm = new FormGroup({
    name: new FormControl('', Validators.required),
    descriptif: new FormControl('', [Validators.required])
  });

  async save() {
    if (this.projectForm.valid) {
      this.usersId = this.route.snapshot.paramMap.get('id') as string
      const values = this.projectForm.value 
      console.log(values)
      const result = {...values} as unknown as Project
      this._projectService.addProject(result, this.usersId).subscribe()
      this._router.navigateByUrl('/');
    }

  }

}
