import { Component, OnInit } from '@angular/core';
import { Project } from '../models/project';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  project! : Project
  id! : string

  constructor(private _projectService : ProjectService, private route : ActivatedRoute, private _router : Router){

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.getProjectById(this.id).subscribe(project => {
      this.project = project
    })
  }

  deleteProject(){
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.deleteProjectById(this.id).subscribe(()=>{
      this._router.navigateByUrl('/')
    }
    )
  }
}
