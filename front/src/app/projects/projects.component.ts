import { Component, OnInit } from '@angular/core';
import { Project } from '../models/project';
import { ProjectService } from '../service/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  
  projects : Project[] = []

  constructor(private _projectService : ProjectService){
  
  }
  
    ngOnInit(): void {
      this._projectService.getProjects().subscribe(projects => {
        this.projects = projects
      })
    }
}
