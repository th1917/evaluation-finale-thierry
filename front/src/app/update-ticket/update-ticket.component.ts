import { Component } from '@angular/core';
import { Ticket } from '../models/ticket';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.scss']
})
export class UpdateTicketComponent {
  ticketId : string = ''
  ticket?: Ticket;

  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  ticketForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl('',Validators.required),
    status: new FormControl('')
  });

  //recupération du film par rapport à son id et affectation des valeur dans le formulaire
  ngOnInit(): void {

    this.ticketId = this.route.snapshot.paramMap.get('id') as string

    this._projectService.getTicketById(this.ticketId).subscribe(ticket => {
      this.ticket = ticket;

      if (ticket) {
        this.ticketForm.patchValue({
          title: this.ticket.title,
          content: this.ticket.content,
          status: this.ticket.status
  
        })
      }
    })

  }

  async save() {
    if (this.ticketForm.valid) {
      const values = this.ticketForm.value 
      const result = {...values} as unknown as Ticket
      this._projectService.updateTicket(result, this.ticketId).subscribe()
      this._router.navigateByUrl('/ticketstatus');
    }
  }



}
