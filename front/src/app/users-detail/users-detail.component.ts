import { Component, OnInit } from '@angular/core';
import { Users } from '../models/users';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit{
  users! : Users
  id! : string

  constructor(private _projectService : ProjectService, private route : ActivatedRoute, private _router : Router){

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.getUsersById(this.id).subscribe(users => {
      this.users = users
    })
  }

  deleteUsers(){
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.deleteUsersById(this.id).subscribe(()=>{
      this._router.navigateByUrl('/users')
    }
    )
  }
}
