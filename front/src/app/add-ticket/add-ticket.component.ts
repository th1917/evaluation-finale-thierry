import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from '../models/ticket';
import { Project } from '../models/project';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.scss']
})
export class AddTicketComponent {
  projectId! : string
  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  ticketForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl('',Validators.required)
  });

  async save() {
    if (this.ticketForm.valid) {
      this.projectId = this.route.snapshot.paramMap.get('id') as string
      const values = this.ticketForm.value 
      const result = {...values} as unknown as Ticket
      this._projectService.addTicket(result, this.projectId).subscribe()
      this._router.navigateByUrl('/');
    }
  }

}
