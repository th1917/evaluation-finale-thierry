export interface Ticket {
    title : string
    content: string
    createdAtTickets : string
    status : string
    id : string
}
