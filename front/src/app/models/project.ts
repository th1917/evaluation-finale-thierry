import { Ticket } from "./ticket"

export interface Project {
    name : string
    descriptif: string
    createdAtProject : string
    tickets : Ticket[]
    id : string
}
