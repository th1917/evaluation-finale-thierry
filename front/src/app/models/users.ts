import { Project } from "./project"

export interface Users {
    firstName : string
    lastName : string
    email : string
    id : string
    projects : Project[]
}
