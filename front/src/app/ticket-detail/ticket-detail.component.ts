import { Component } from '@angular/core';
import { Ticket } from '../models/ticket';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent {
  ticket! : Ticket
  id! : string

  constructor(private _projectService : ProjectService, private route : ActivatedRoute, private _router : Router){

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.getTicketById(this.id).subscribe(ticket => {
      this.ticket = ticket
    })
  }

  deleteTicket(){
    this.id = this.route.snapshot.paramMap.get('id') as string
    this._projectService.deleteTicketById(this.id).subscribe(()=>{
      this._router.navigateByUrl('/ticketstatus')
    }
    )
  }
}
