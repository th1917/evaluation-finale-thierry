import { Component, Input, OnInit } from '@angular/core';
import { Ticket } from '../models/ticket';
import { ProjectService } from '../service/project.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit{
  @Input() data! : Ticket

  TODOTickets       : Ticket[] = []
  INPROGRESSTickets : Ticket[] = []
  DONETickets       : Ticket[] = []

constructor(private _projectService : ProjectService){

}

  ngOnInit(): void {
    this._projectService.getTicketTODO().subscribe(tickets => {
      this.TODOTickets = tickets
    })
    this._projectService.getTicketINPROGRESS().subscribe(tickets => {
      this.INPROGRESSTickets = tickets
    })
    this._projectService.getTicketDONE().subscribe(tickets => {
      this.DONETickets = tickets
    })
  }
}
