import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project';
import { Observable, Subject, tap } from 'rxjs';
import { Ticket } from '../models/ticket';
import { Users } from '../models/users';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  autocompletion: string = '';
  projects : Project[] = [];
  users : Users[] = [];

  TODOTickets       : Ticket[] = []
  INPROGRESSTickets : Ticket[] = []
  DONETickets       : Ticket[] = []

  public subject = new Subject<string>(); 
  
   //Gestion projets
  constructor(private _httpClient: HttpClient) { }

  getProjects(): Observable<Project[]> {
    return this._httpClient.get<Project[]>("/api/projects")
    .pipe(
      tap((projects: Project[]) => {
        this.projects = projects;
      })
    );
  }

// permet de recherche un projet en fonction du mot entré
searchProject(recherche :string):Observable<Project[]> {
  return this._httpClient.get<Project[]>(`/api/projects/search?name=${recherche}`);
}

getProjectById(id: string){
  if (!id || id.length === 0) {
    throw new Error("Mauvais id");

  }
  return this._httpClient.get<Project>(`api/projects/${id}`)
}

addProject(project : Project, id : string):Observable<Project | undefined>{
  return this._httpClient.post<Project>(`api/users/${id}/projects`, project)
}

updateProject(project : Partial<Project>, projectId:string) {
  return this._httpClient.patch<Ticket>(`api/tickets/${projectId}`, project)
}

deleteProjectById(id: string){
  if (!id || id.length === 0) {
    throw new Error("Mauvais id");

  }
  return this._httpClient.delete<Project>(`api/projects/${id}`)
}


  //Gestion des tickets

  getTicketById(id: string){
    if (!id || id.length === 0) {
      throw new Error("Mauvais id");

    }
    return this._httpClient.get<Ticket>(`/api/tickets/${id}`)
  }


  getTicketTODO(): Observable<Ticket[]> {
    return this._httpClient.get<Ticket[]>("/api/tickets/search?status=TODO")
    .pipe(
      tap((TODOTickets: Ticket[]) => {
        this.TODOTickets = TODOTickets;
      })
    );
  }

  getTicketINPROGRESS(): Observable<Ticket[]> {
    return this._httpClient.get<Ticket[]>("/api/tickets/search?status=INPROGRESS")
    .pipe(
      tap((INPROGRESSTickets: Ticket[]) => {
        this.INPROGRESSTickets = INPROGRESSTickets;
      })
    );
  }

  getTicketDONE(): Observable<Ticket[]> {
    return this._httpClient.get<Ticket[]>("/api/tickets/search?status=DONE")
    .pipe(
      tap((DONETickets: Ticket[]) => {
        this.DONETickets = DONETickets;
      })
    );
  }

  addTicket(ticket : Ticket, id: string):Observable<Ticket | undefined>{
    return this._httpClient.post<Ticket>(`api/projects/${id}/tickets`, ticket)

  }

  updateTicket(ticket : Partial<Ticket>, ticketId:string) {
    return this._httpClient.patch<Ticket>(`api/tickets/${ticketId}`, ticket)
  }

  deleteTicketById(id: string){
    if (!id || id.length === 0) {
      throw new Error("Mauvais id");

    }
    return this._httpClient.delete<Ticket>(`api/tickets/${id}`)
  }

  //Gestion des Users

  getUsers(): Observable<Users[]> {
    return this._httpClient.get<Users[]>("/api/users")
    .pipe(
      tap((users: Users[]) => {
        this.users = users;
      })
    );
  }

  getUsersById(id: string){
    if (!id || id.length === 0) {
      throw new Error("Mauvais id");
  
    }
    return this._httpClient.get<Users>(`api/users/${id}`)
  }

  // permet de recherche les projets d'un utilisateur
searchProjectUsers(recherche :string):Observable<Project[]> {
  return this._httpClient.get<Project[]>(`/api/users/${recherche}/projects`);
}

addUsers(users : Users):Observable<Users | undefined>{
  return this._httpClient.post<Users>(`api/users`, users)

}

updateUsers(users : Partial<Users>, usersId:string) {
  return this._httpClient.patch<Users>(`api/tickets/${usersId}`, users)
}



deleteUsersById(id: string){
  if (!id || id.length === 0) {
    throw new Error("Mauvais id");

  }
  return this._httpClient.delete<Users>(`api/users/${id}`)
}

}
