import { Component } from '@angular/core';
import { Project } from '../models/project';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent {

  projectId : string = ''
  project?: Project;

  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  projectForm = new FormGroup({
    descriptif: new FormControl('',Validators.required)
  });

  //recupération d'un project par rapport à son id et affectation des valeur dans le formulaire
  ngOnInit(): void {

    this.projectId = this.route.snapshot.paramMap.get('id') as string

    this._projectService.getProjectById(this.projectId).subscribe(project => {
      this.project = project;

      if (project) {
        this.projectForm.patchValue({
          descriptif: this.project.descriptif
        })
      }
    })

  }

  async save() {
    if (this.projectForm.valid) {
      const values = this.projectForm.value 
      const result = {...values} as unknown as Project
      this._projectService.updateProject(result, this.projectId).subscribe()
      this._router.navigateByUrl('/');
    }
  }
}
