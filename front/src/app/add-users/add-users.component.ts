import { Component } from '@angular/core';
import { Users } from '../models/users';
import { ProjectService } from '../service/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent {
  usersId : string = ''

  constructor(private _projectService: ProjectService, private _router: Router, private route : ActivatedRoute) { }

  usersForm = new FormGroup({
    firstName: new FormControl('',Validators.required),
    lastName: new FormControl(''),
    email: new FormControl('',Validators.required)
  });

  async save() {
    if (this.usersForm.valid) {
      const values = this.usersForm.value 
      const result = {...values} as unknown as Users
      this._projectService.addUsers(result).subscribe()
      this._router.navigateByUrl('/users');
    }
  }
}
