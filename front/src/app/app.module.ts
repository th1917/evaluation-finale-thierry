import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { registerLocaleData } from '@angular/common';
import localeFr from "@angular/common/locales/fr";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchBarComponent } from './search-bar/search-bar.component';
import { HomeComponent } from './home/home.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { ProjectsComponent } from './projects/projects.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { UpdateUsersComponent } from './update-users/update-users.component';


registerLocaleData(localeFr);

@NgModule({
  declarations: [AppComponent, HomeComponent,SearchBarComponent, TicketListComponent, ProjectListComponent, AddProjectComponent, TicketDetailComponent, ProjectsComponent, AddTicketComponent, ProjectDetailComponent, UsersDetailComponent, UsersListComponent, UpdateTicketComponent, UpdateProjectComponent, AddUsersComponent, UpdateUsersComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
