import { Component, OnInit } from '@angular/core';
import { Users } from '../models/users';
import { ProjectService } from '../service/project.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit{

  users : Users[] = []

  constructor(private _projectService : ProjectService){
  
  }
  
    ngOnInit(): void {
      this._projectService.getUsers().subscribe(users => {
        this.users = users
      })
    }

}
