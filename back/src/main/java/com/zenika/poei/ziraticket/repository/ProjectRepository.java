package com.zenika.poei.ziraticket.repository;

import com.zenika.poei.ziraticket.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    List<Project> findByNameContaining(String name);
    List<Project> findByUsersId(Integer usersId);

}
