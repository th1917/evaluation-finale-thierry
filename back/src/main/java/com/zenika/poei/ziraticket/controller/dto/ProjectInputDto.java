package com.zenika.poei.ziraticket.controller.dto;

import com.zenika.poei.ziraticket.domain.Project;
import com.zenika.poei.ziraticket.domain.Ticket;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.ColumnTransformer;

public class ProjectInputDto {
    @ColumnTransformer(read = "name", write = "LOWER(?)")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    @Size(max = 50)
    private String descriptif;

    public Project toDomains() {
        Project project = new Project(name,descriptif);
        project.setName(name);
        project.setDescriptif(descriptif);
        return project;
    }

}
