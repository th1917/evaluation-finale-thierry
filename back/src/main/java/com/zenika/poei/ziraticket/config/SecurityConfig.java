package com.zenika.poei.ziraticket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()

                .requestMatchers(HttpMethod.POST, "/api/projects/*/tickets/**").hasRole("USER")
                .requestMatchers(HttpMethod.POST, "/api/users/*/projects/**").hasRole("USER")
                .requestMatchers(HttpMethod.GET, "/api/users/*/projects/**").hasRole("USER")
                .requestMatchers(HttpMethod.PUT, "/api/projects/**").hasRole("USER")
                .requestMatchers(HttpMethod.PUT, "/api/tickets/**").hasRole("USER")
                .requestMatchers(HttpMethod.GET, "/api/users/**").hasAnyRole("USER", "ADMIN")
                .requestMatchers(HttpMethod.DELETE, "/api/users/**").hasRole("ADMIN")
                .requestMatchers(HttpMethod.DELETE, "/api/projects/**").hasRole("USER")
                .requestMatchers(HttpMethod.DELETE, "/api/tickets/**").hasRole("USER")
                .requestMatchers(HttpMethod.PATCH, "/api/users/**").hasRole("USER")
                .requestMatchers(HttpMethod.PATCH, "/api/projects/**").hasRole("USER")
                .requestMatchers(HttpMethod.PATCH, "/api/tickets/**").hasRole("USER")
                // accept swagger-ui public
                .requestMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                // Any other request needs the user to be authenticated first, i.e. the user needs to login.
                .anyRequest().permitAll()
                .and()
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                // Without Basic realm header in order to avoid internet browser's popup
                .httpBasic(withDefaults())
                // CSRF protection is enabled by default in the Java configuration.
                .csrf().disable()
                // The strictest session creation option – “stateless” – is a guarantee that the application will not create any session at all.
                // No COOKIE !!!!
                .sessionManagement(httpSecuritySessionManagementConfigurer -> httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        User.UserBuilder users = User.withDefaultPasswordEncoder(); //Créer des bean pour les autorisés
        UserDetails visiter = users
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        UserDetails user = users
                .username("admin")
                .password("password")
                .roles("USER", "ADMIN")
                .build();

        return new InMemoryUserDetailsManager(visiter, user);
    }

}