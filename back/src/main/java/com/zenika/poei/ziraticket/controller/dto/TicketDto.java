package com.zenika.poei.ziraticket.controller.dto;

import java.time.LocalDateTime;

public class TicketDto {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String title;


    private String content;

    private LocalDateTime createdAtTicket;

    private String status;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreatedAtTicket() {
        return createdAtTicket;
    }

    public void setCreatedAtTicket(LocalDateTime createdAtTicket) {
        this.createdAtTicket = createdAtTicket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
