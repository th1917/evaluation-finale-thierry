package com.zenika.poei.ziraticket.domain.exception;

public class InvalidUsersException extends InvalidException{

    public InvalidUsersException() {
    }

    public InvalidUsersException(String message) {
        super(message);
    }

    public InvalidUsersException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUsersException(Throwable cause) {
        super(cause);
    }
}
