package com.zenika.poei.ziraticket.Mapper;


import com.zenika.poei.ziraticket.controller.dto.TicketDto;

import com.zenika.poei.ziraticket.controller.dto.TicketInputDto;
import com.zenika.poei.ziraticket.domain.Ticket;
import com.zenika.poei.ziraticket.service.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
@Mapper(componentModel = "spring")
public interface TicketMapper {

    TicketDto ticketToTicketDto (Ticket ticket);

    List<TicketDto> getTicketDto(List<Ticket> tickets);
}
