package com.zenika.poei.ziraticket.domain.exception;

public class NotFoundException extends FunctionalException{


    private final Integer id;
    private final Class<?> entityClass;

    public NotFoundException(Class<?> entityClass, Integer id) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found");
        this.id = id;
        this.entityClass = entityClass;
    }
}
