package com.zenika.poei.ziraticket.repository;

import com.zenika.poei.ziraticket.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
}
