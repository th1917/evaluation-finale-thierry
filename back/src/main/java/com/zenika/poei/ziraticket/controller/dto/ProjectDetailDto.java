package com.zenika.poei.ziraticket.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ProjectDetailDto {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String name;

    private String descriptif;

    private LocalDateTime createdAtProject;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }



    public LocalDateTime getCreatedAtProject() {
        return createdAtProject;
    }
    public void setCreatedAtProject(LocalDateTime createdAtProject) {
        this.createdAtProject = createdAtProject;
    }

    private List<TicketDto> tickets = new ArrayList<>();

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
    }


}
