package com.zenika.poei.ziraticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZiraTicket {

    public static void main(String[] args) {
        SpringApplication.run(ZiraTicket.class, args);
    }

}

