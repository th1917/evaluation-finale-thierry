package com.zenika.poei.ziraticket.repository;
import com.zenika.poei.ziraticket.domain.Project;
import com.zenika.poei.ziraticket.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    List<Ticket> findByStatusContaining(String status);
}
