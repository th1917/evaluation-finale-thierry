package com.zenika.poei.ziraticket.controller.dto;

import com.zenika.poei.ziraticket.domain.Ticket;
import jakarta.validation.constraints.Size;

public class TicketInputDto {


    @Size(max = 50)
    private String title;

    @Size(max = 1000)
    private String content;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Ticket toDomain() {
        Ticket ticket = new Ticket(title, content);
        ticket.setTitle(title);
        ticket.setContent(content);
        ticket.setStatus(status);
         return ticket;
    }
}
