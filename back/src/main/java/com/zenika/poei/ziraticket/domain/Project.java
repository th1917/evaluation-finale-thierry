package com.zenika.poei.ziraticket.domain;

import com.zenika.poei.ziraticket.domain.exception.InvalidTicketException;
import jakarta.persistence.*;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.OrderBy;


import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.time.LocalDateTime.now;


@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ColumnTransformer(read = "name", write = "LOWER(?)")
    private String name;

    public Project() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String descriptif;

    @Column(name = "created_at_project")
    private LocalDateTime createdAtProject;

    @OneToMany(mappedBy = "projectId", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy(clause = "created_at_ticket DESC")
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @Column(name = "users_id")
    private Integer usersId;

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    public LocalDateTime getCreatedAtProject() {
        return createdAtProject;
    }

    public void setCreatedAtProject(LocalDateTime createdAtProject) {
        this.createdAtProject = createdAtProject;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @PrePersist //La date de création ne doit pas être fournie par l'utilisateur.
    public void initDateCreation() {
        createdAtProject = now();
    }

    public Project(String name, String descriptif) {
        this.name = name;
        this.descriptif = descriptif;
    }

    public void addTicket(Ticket ticket) {
        if (ticket == null) {
            throw new InvalidTicketException("Ticket is undefined");
        }

        // Check that title don't already exist in tickets of projects
        for (Ticket existinTicket : tickets) {
            if (Objects.equals(existinTicket.getTitle(), ticket.getTitle())) {
                throw new InvalidTicketException("Title " + ticket.getTitle() + " is already in ticketList of project " + id);
            }
        }
        // Save current date
        ticket.setCreatedAtTicket(now());

        tickets.add(ticket);
    }
}


