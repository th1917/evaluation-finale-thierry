package com.zenika.poei.ziraticket.domain.exception;

public class InvalidProjectException extends InvalidException{
    public InvalidProjectException() {
    }

    public InvalidProjectException(String message) {
        super(message);
    }

    public InvalidProjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidProjectException(Throwable cause) {
        super(cause);
    }
}
