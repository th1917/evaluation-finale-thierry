package com.zenika.poei.ziraticket.service;
import com.zenika.poei.ziraticket.controller.dto.Status;
import com.zenika.poei.ziraticket.domain.Project;
import com.zenika.poei.ziraticket.domain.Ticket;
import com.zenika.poei.ziraticket.domain.Users;
import com.zenika.poei.ziraticket.domain.exception.FunctionalException;
import com.zenika.poei.ziraticket.domain.exception.NotFoundException;
import com.zenika.poei.ziraticket.repository.ProjectRepository;
import com.zenika.poei.ziraticket.repository.TicketRepository;
import com.zenika.poei.ziraticket.repository.UsersRepository;

import org.springframework.stereotype.Service;


import java.util.List;


@Service
public class ProjectService {


    private final ProjectRepository repository;

    private final TicketRepository ticketRepository;

    private final UsersRepository usersRepository;

    public ProjectService(ProjectRepository repository, TicketRepository ticketRepository, UsersRepository usersRepository) {
        this.repository = repository;
        this.ticketRepository = ticketRepository;
        this.usersRepository = usersRepository;

    }
//Tout ce qui concerne le service des utilisateurs
    public List<Users> listUsers() {
        return usersRepository.findAll();
    }

    public Users createUser(Users users){return usersRepository.save(users);}

    public Users getUser(Integer usersId) {
        return usersRepository.findById(usersId)
                .orElseThrow(() -> new NotFoundException(Users.class, usersId));
    }
    public boolean existingUser(Users users){
        boolean usersExist = false;

        List<Users> useres = usersRepository.findAll();

        for (Users uses: useres) {
            if(uses.getEmail().equals(users.getEmail())){
                usersExist = true;
                break;
            }
        }
        return usersExist;
    }

    public Users updatePartialUsers(Integer usersId, Users users){
        Users existigUsers = usersRepository.findById(usersId)
                .orElseThrow(() -> new NotFoundException(Users.class, usersId));;

        if (!users.getFirstName().isEmpty()) {
            existigUsers.setFirstName(users.getFirstName());
        }else {
            existigUsers.setFirstName(existigUsers.getFirstName());
        }

        if (!users.getLastName().isEmpty()) {
            existigUsers.setLastName(users.getLastName());
        }else{
            existigUsers.setLastName(existigUsers.getLastName());
        }
        return usersRepository.save(existigUsers);
    }

    public void deleteUsers(Integer usersId) {
        Users users = usersRepository.findById(usersId)
                .orElseThrow(() -> new NotFoundException(Users.class, usersId));
        usersRepository.delete(users);
    }


//Tout ce qui concerne le service des projects
    public List<Project> listProjects() {
        return repository.findAll();
    }


    public Project getProject(Integer projectId) {
        return repository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));
    }

    public Project createProject(Project project) {
        return repository.save(project);
    }

    public boolean existingProject(Integer usersId, Project project){
        boolean projectExist = false;

        Users users = usersRepository.findById(usersId).orElseThrow(() -> new NotFoundException(Users.class, usersId));

        List<Project> projects = users.getProjects();

        for (Project proj: projects) {
            if(proj.getName().equals(project.getName())){
                projectExist = true;
                break;
            }
        }
        return projectExist;
    }

    public boolean updateProject(Integer projectId, Project project) {
        boolean existupdateProject = true;
        Project existProject = repository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));
        if(!existProject.getName().equals(project.getName())) {
            existProject.setName(project.getName());
            existProject.setDescriptif(project.getDescriptif());
            repository.save(existProject);
            existupdateProject = false;
        }
        return existupdateProject;
    }

    public Project updatePartialProject(Integer projectId, Project project){
        Project existigProject = repository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));;

        if (!project.getDescriptif().isEmpty()) {
            existigProject.setDescriptif(project.getDescriptif());
        }else{
            existigProject.setDescriptif(existigProject.getDescriptif());
        }

        return repository.save(existigProject);
    }

    public List<Project> searchByName(String name) {
        return repository.findByNameContaining(name);
    }

    public List<Project> searchByUserId(Integer usersId) {
        if (usersId != null) {
            return repository.findByUsersId(usersId);
        } else {
            return null;
        }
    }

    public void deleteProject(Integer projectId) {
        Project project = repository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(Project.class, projectId));
        repository.delete(project);
    }





//Tout ce qui concerne les tickets
    public Ticket getTicket(Integer ticketId) {
        return ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Ticket.class, ticketId));
    }

    public void createTicket(Ticket ticket){
        ticketRepository.save(ticket);
    }

    public boolean updateTicket(Integer ticketId, Ticket ticket) {
        boolean existupdateTicket = true;
        Ticket existTicket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Ticket.class, ticketId));
        if(!existTicket.getTitle().equals(ticket.getTitle())){
            existTicket.setTitle(ticket.getTitle());
            existTicket.setContent(ticket.getContent());
            existTicket.setStatus(ticket.getStatus());
            ticketRepository.save(existTicket);
            existupdateTicket = false;
        }

        return existupdateTicket;
    }

    public Ticket updatePartialTicket(Integer ticketId, Ticket ticket){
        Ticket existigTicket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Ticket.class, ticketId));;

        if (!ticket.getContent().isEmpty()) {
            existigTicket.setContent(ticket.getContent());
        }else{
            existigTicket.setContent(existigTicket.getContent());
        }

        if (!ticket.getStatus().isEmpty() && (ticket.getStatus().equalsIgnoreCase(Status.TODO.name()) || (ticket.getStatus().equalsIgnoreCase(Status.INPROGRESS.name()))|| (ticket.getStatus().equalsIgnoreCase(Status.DONE.name())))) {
            existigTicket.setStatus(ticket.getStatus());
        } else{
            existigTicket.setStatus(existigTicket.getStatus());
            System.out.println("Entrer un status : TODO, INPROGRESS, DONE");

        }

        return ticketRepository.save(existigTicket);
    }

    public boolean existingTicket(Integer projectId, Ticket ticket){
        boolean ticketExist = false;

        Project project = repository.findById(projectId).orElseThrow(() -> new NotFoundException(Project.class, projectId));

        List<Ticket> tickets = project.getTickets();

        for (Ticket tic: tickets) {
            if(tic.getTitle().equals(ticket.getTitle())){
                ticketExist = true;
                break;
            }
        }
        return ticketExist;
    }

    public void deleteTicket(Integer ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Project.class, ticketId));
        ticketRepository.delete(ticket);
    }

    public List<Ticket> searchByStatus(String status) {
        return ticketRepository.findByStatusContaining(status);
    }



}
