package com.zenika.poei.ziraticket.domain;

import jakarta.persistence.*;
import org.hibernate.annotations.DynamicInsert;

import java.time.LocalDateTime;

//@Embeddable
@Entity
@DynamicInsert
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String title;


    private String content;


    @Column(name =" created_at_ticket")
    private LocalDateTime createdAtTicket;

    private String status;


    @Column(name = "project_id")
    private Integer projectId;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @PrePersist //La date de création ne doit pas être fournie par l'utilisateur.
    public void initDateCreation() {
        createdAtTicket = LocalDateTime.now();
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAtTicket() {
        return createdAtTicket;
    }

    public void setCreatedAtTicket(LocalDateTime createdAtTicket) {
        this.createdAtTicket = createdAtTicket;
    }


    public Ticket(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Ticket() {
    }
}
