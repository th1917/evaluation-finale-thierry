package com.zenika.poei.ziraticket.Mapper;


import com.zenika.poei.ziraticket.controller.dto.*;
import com.zenika.poei.ziraticket.domain.Project;
import com.zenika.poei.ziraticket.domain.Ticket;
import com.zenika.poei.ziraticket.service.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = TicketMapper.class)
public interface ProjectMapper {
    ProjectDto projectToProjectDto (Project project);
    ProjectDetailDto projectToProjectDetailDto (Project project);
    List<ProjectDetailDto> getProjectDetailDto(List<Project> projects);
    List<ProjectDto> getProjectDto(List<Project> projects);
}
