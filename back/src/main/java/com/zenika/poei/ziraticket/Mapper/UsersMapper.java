package com.zenika.poei.ziraticket.Mapper;
import com.zenika.poei.ziraticket.controller.dto.UsersDto;
import com.zenika.poei.ziraticket.controller.dto.UsersInputDto;
import com.zenika.poei.ziraticket.controller.dto.UsersDetailDto;
import com.zenika.poei.ziraticket.domain.Users;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapper {
    UsersDto usersToUsersDto(Users users);
    UsersDetailDto usersToUsersDetailDto(Users users);
    Users usersInputDtoToUsers(UsersInputDto usersInputDto);
    List<UsersDetailDto> getUsersDetailDto(List<Users> users);
}
