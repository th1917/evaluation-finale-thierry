package com.zenika.poei.ziraticket.controller;

import com.zenika.poei.ziraticket.Mapper.ProjectMapper;
import com.zenika.poei.ziraticket.Mapper.TicketMapper;
import com.zenika.poei.ziraticket.Mapper.UsersMapper;
import com.zenika.poei.ziraticket.controller.dto.*;

import com.zenika.poei.ziraticket.domain.Project;
import com.zenika.poei.ziraticket.domain.Ticket;
import com.zenika.poei.ziraticket.domain.Users;
import com.zenika.poei.ziraticket.service.ProjectService;

import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/api")
public class ProjectController {

    @Autowired
    private ProjectService service;

    @Autowired
    private ProjectMapper mapProject;

    @Autowired
    private TicketMapper mapTicket;

    @Autowired
    private UsersMapper mapUsers;


//Tout ce qui concerne le Users
    /**
     * Retourne la liste des users.
     * @param
     * @return
     */

    @GetMapping("/users")
    public ResponseEntity<List<UsersDetailDto>> findUser() {
        List<UsersDetailDto> findUsersDetailDto = mapUsers.getUsersDetailDto(service.listUsers());
        return ResponseEntity.ok(findUsersDetailDto);
    }

    /**
     * Retourne l'user ayant l'id passé en paramètre.
     * @param usersId
     * @return
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<UsersDetailDto> getUserById(@PathVariable("id") Integer usersId) {
        UsersDetailDto usersDetailDto = mapUsers.usersToUsersDetailDto(service.getUser(usersId));
        return ResponseEntity.ok(usersDetailDto);
    }

    /**
     * Ajoute un nouveau  users dans la base de données Postgres. Si le users existe déjà, on retourne un code indiquant que l'ajout n'a pas abouti.
     * @param usersInputDto
     * @return
     */

    @PostMapping("/users")
    public ResponseEntity<UsersDto> createNewUser(@RequestBody @Valid UsersInputDto usersInputDto) {

        boolean userExist = service.existingUser(mapUsers.usersInputDtoToUsers(usersInputDto));

        if (!userExist) {

            UsersDto usersDto = mapUsers.usersToUsersDto(service.createUser(mapUsers.usersInputDtoToUsers(usersInputDto)));

            return new ResponseEntity<UsersDto>(usersDto, HttpStatus.CREATED);
        }

        return new ResponseEntity<UsersDto>(HttpStatus.NOT_MODIFIED);
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<UsersDto> updateUserPartially(@PathVariable("id") Integer usersInputDtoId, @RequestBody UsersInputDto usersInputDto) {
        UsersDto usersDto = mapUsers.usersToUsersDto(service.updatePartialUsers(usersInputDtoId, mapUsers.usersInputDtoToUsers(usersInputDto)));
        return new ResponseEntity<UsersDto>(usersDto, HttpStatus.CREATED);
    }

    /**
     * Suppression d'un user.
     * @param
     * @return
     */
    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUsers(@PathVariable("id") Integer usersId) {
        service.deleteUsers(usersId);
    }



//Tout ce qui concerne les projets
    /**
     * Retourne la liste des projets et leurs tickets.
     * @param
     * @return
     */
    @GetMapping("/projects")
    public ResponseEntity<List<ProjectDetailDto>> findProject() {
        List<ProjectDetailDto> findProjectDetailDto = mapProject.getProjectDetailDto(service.listProjects());
        return ResponseEntity.ok(findProjectDetailDto);
    }

    /**
     * Retourne la liste des projects ayant l'id passé en paramètre.
     * @param projectId
     * @return
     */
    @GetMapping("/projects/{id}")
    public ResponseEntity<ProjectDto> getProjectById(@PathVariable("id") Integer projectId) {
        ProjectDto projectDto = mapProject.projectToProjectDto(service.getProject(projectId));
        return ResponseEntity.ok(projectDto);
    }


    /**
     * Retourne la liste des projets ayant le name passé en paramètre.
     * @param name
     * @return
     */

    @GetMapping("/projects/search")
    public ResponseEntity<List<ProjectDto>> searchByName(@RequestParam("name") String name) {

        List<ProjectDto> projectsDto= mapProject.getProjectDto(service.searchByName(name));
        return new ResponseEntity<List<ProjectDto>>(projectsDto, HttpStatus.OK);
    }

    /**
     * Retourne la liste des projets d'un user.
     * @param usersId
     * @return
     */
    @GetMapping("/users/{id}/projects")
    public List<ProjectDto> searchByUsers(@PathVariable(value = "id") Integer usersId) {

        return mapProject.getProjectDto(service.searchByUserId(usersId));
    }

    @PostMapping("/users/{id}/projects")
    public ResponseEntity<ProjectDto> addProject(@PathVariable("id") Integer usersId, @RequestBody @Valid ProjectInputDto projectInputDto) {
        boolean projectExiste = service.existingProject(usersId, projectInputDto.toDomains());

        if (!projectExiste) {
            Project project = projectInputDto.toDomains();
            project.setUsersId(usersId);
            service.createProject(project);
            ProjectDto projectDto = mapProject.projectToProjectDto(project);
            return new ResponseEntity<ProjectDto>(projectDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<ProjectDto>( HttpStatus.NOT_MODIFIED);
    }

    /**
     * Modification d'un project d'id dans la base de données Postgre.
     * @param projectInputDto
     * @return
     */
    @PutMapping("/projects/{id}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable("id") Integer projectId, @RequestBody @Valid ProjectInputDto projectInputDto) {
        boolean projectUpdate = service.updateProject(projectId, projectInputDto.toDomains());

        ProjectDto projectDto = mapProject.projectToProjectDto(projectInputDto.toDomains());

        if (!projectUpdate) {
            return new ResponseEntity<ProjectDto>(projectDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<ProjectDto>(HttpStatus.NOT_MODIFIED);
    }

    @PatchMapping("/projects/{id}")
    public ResponseEntity<ProjectDto> updateProjectPartially(@PathVariable("id") Integer projectInputDtoId, @RequestBody ProjectInputDto projectInputDto) {
        ProjectDto projectDto = mapProject.projectToProjectDto(service.updatePartialProject(projectInputDtoId, projectInputDto.toDomains()));
        return new ResponseEntity<ProjectDto>(projectDto, HttpStatus.CREATED);
    }

    /**
     * Suppression d'un project et des tickets associés ticket  dans la base de données Postgre.
     * @param "id"
     * @return
     */
    @DeleteMapping("/projects/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable("id") Integer projectId) {
        service.deleteProject(projectId);
    }


//Tout ce qui concerne les tickets
    /**
     * Retourne la liste des tickects ayant le status passé en paramètre.
     * @param status
     * @return
     */

    @GetMapping("/tickets/search")
    public List<TicketDto> searchByStatus(@RequestParam("status") String status) {
        return mapTicket.getTicketDto(service.searchByStatus(status));
    }

    /**
     * Retourne un tickect ayant l'id passé en paramètre.
     * @param ticketId
     * @return
     */
    @GetMapping("tickets/{id}")
    public ResponseEntity<TicketDto> getTicketById(@PathVariable("id") Integer ticketId) {
        TicketDto ticketDto = mapTicket.ticketToTicketDto(service.getTicket(ticketId));

        return ResponseEntity.ok(ticketDto);
    }

    /**
     * Ajoute un nouveau  ticket du projet d'id dans la base de données Postgre. Si le ticket existe déjà, on retourne un code indiquant que l'ajout n'a pas abouti.
     * @param ticketInputDto
     * @return
     */
    @PostMapping("/projects/{id}/tickets")
    public ResponseEntity<TicketDto> addTickets(@PathVariable("id") Integer projectId, @RequestBody @Valid TicketInputDto ticketInputDto) {
        boolean ticketExiste = service.existingTicket(projectId, ticketInputDto.toDomain());

        if (!ticketExiste) {
            Ticket ticket = ticketInputDto.toDomain();
            ticket.setProjectId(projectId);
            service.createTicket(ticket);
            TicketDto ticketDto = mapTicket.ticketToTicketDto(ticket);
            return new ResponseEntity<TicketDto>(ticketDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<TicketDto>(HttpStatus.NOT_MODIFIED);
    }

    @PutMapping("/tickets/{id}")
    public ResponseEntity<TicketDto> updateTickets(@PathVariable("id") Integer ticketId, @RequestBody @Valid TicketInputDto ticketInputDto) {
        boolean ticketUpdate = service.updateTicket(ticketId, ticketInputDto.toDomain());

        TicketDto ticketDto = mapTicket.ticketToTicketDto(ticketInputDto.toDomain());

        if (!ticketUpdate) {
            return new ResponseEntity<TicketDto>(ticketDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<TicketDto>(HttpStatus.NOT_MODIFIED);

    }


    @PatchMapping("/tickets/{id}")
    public ResponseEntity<TicketDto> updateTicketPartially(@PathVariable("id") Integer ticketInputDtoId, @RequestBody TicketInputDto ticketInputDto) {
        TicketDto ticketDto = mapTicket.ticketToTicketDto(service.updatePartialTicket(ticketInputDtoId, ticketInputDto.toDomain()));
        return new ResponseEntity<TicketDto>(ticketDto, HttpStatus.CREATED);
    }

    /**
     * Suppression d'un ticket.
     * @param
     * @return
     */
    @DeleteMapping("/tickets/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTicket(@PathVariable("id") Integer ticketId) {
        service.deleteTicket(ticketId);
    }




}
