package com.zenika.poei.ziraticket.domain;

import com.zenika.poei.ziraticket.domain.exception.InvalidTicketException;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TicketTest {
    @Test
    void shouldCreateTicket() {
        Ticket ticket = new Ticket("developpeur", "Excellent");

        assertThat(ticket).isNotNull();
    }

    @Test
    void shouldAddOneTicketToProject() {
        // Given
        Project project = new Project("capgemini", "nous avons besoin de gèrer la connection de nos utilisateurs");

        // When
        project.addTicket(new Ticket("developpeur", "Excellent"));

        // Then
        assertThat(project.getName()).isEqualTo("capgemini");
    }
    @Test
    void shouldRejectDoubleTicketFromSameTitle() {
        // Given
        Project project = new Project("capgemini", "nous avons besoin de gèrer la connection de nos utilisateurs");


        // When
        project.addTicket(new Ticket("informatique", "Très bon"));

        // Then
        assertThatThrownBy(() -> project.addTicket(new Ticket("informatique","En fait, il est excellent")))
                .isInstanceOf(InvalidTicketException.class);
    }
}
