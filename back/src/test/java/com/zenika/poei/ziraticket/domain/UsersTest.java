package com.zenika.poei.ziraticket.domain;

import com.zenika.poei.ziraticket.domain.exception.InvalidUsersException;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class UsersTest {

    @Test
    void shouldCreateUsers() {
        Users users = new Users("bernard", "Henri", "bernard@yahoo.fr");

        assertThat(users).isNotNull();
    }
/*
    @Test
    void shouldRejectEmptyFirstName() {
        assertThatThrownBy(() -> new Users("", "henrietta", "thierry@yahoo.fr"))
                .isInstanceOf(InvalidUsersException.class);
    }
    */
}
